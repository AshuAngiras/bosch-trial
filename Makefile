#Setup everything in one call
setup: cluster test docker release

#build binary 
buildall : test build

#Create KIND k8 cluster with registry and ingress enabled
cluster:
	kind create cluster --name homecluster --config infra/kind.yaml || true
	sh infra/registry.sh
	sh infra/ingress.sh

#Build docker and push image 
docker:
	docker build -t localhost:5000/whattimeisit:1.0.4 .
	docker push localhost:5000/whattimeisit:1.0.4

test: 
	go mod tidy
	go test ./... -cover
build: 
	go mod tidy
	go build -o bin cmd/whatTimeIsIt/main.go

#Deploy the app
release:
	terraform -chdir=deploy init 
	terraform -chdir=deploy apply -auto-approve 

#Run app locally 
app:
	go mod tidy
	go run cmd/whatTimeIsIt/main.go
	
#delete all created resources 
destroy:
	terraform -chdir=deploy destroy -auto-approve || exit 0
	kind delete cluster --name homecluster || exit 0