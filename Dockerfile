
ARG  BUILDER_IMAGE=golang:alpine
FROM ${BUILDER_IMAGE} as builder


RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"
WORKDIR $GOPATH/src/whatTimeIsIt
COPY . .

RUN go mod tidy

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -ldflags='-w -s -extldflags "-static"' -a \
    -o /go/bin/whattimeisit ./cmd/whatTimeIsIt

#####################################################

FROM alpine

RUN apk add --no-cache tzdata
ENV TZ Europe/Berlin

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

COPY --from=builder /go/bin/whattimeisit /go/bin/whattimeisit
EXPOSE 8001

USER appuser:appuser

ENTRYPOINT ["/go/bin/whattimeisit"]
