package main

import (
	"log"

	"gitlab.com/AshuAngiras/bosch-trial/pkg/service"
)

func main() {
	log.Println("Ready to find out what TIME is it?")

	service.Start()
}
