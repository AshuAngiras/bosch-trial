resource "kubernetes_service" "whattimeisit" {
  metadata {
    name = "whattimeisit"
    namespace = "main"
  }
  spec {
    selector = {
      app = kubernetes_deployment.whattimeisit.metadata.0.name
    }
    port {
      port        = 8001
      target_port = 8001
    }

    type = "ClusterIP"
  }
}
