resource "kubernetes_ingress" "ingress" {
  metadata {
    name = "ingress"
    namespace = "main"
  }

  spec {
    rule {
      http {
        path {
          backend {
            service_name = kubernetes_service.whattimeisit.metadata.0.name
            service_port = 8001
          }
          path = "/"
        }
      }
    }
  }
}