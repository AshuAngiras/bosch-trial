resource "kubernetes_deployment" "whattimeisit" {
  metadata {
    name = "whattimeisit"
    namespace = "main"
    labels = {
      app = "whattimeisit"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "whattimeisit"
      }
    }

    template {
      metadata {
        labels = {
          app = "whattimeisit"
        }
      }

      spec {
        container {
          image = "localhost:5000/whattimeisit:1.0.4"
          name  = "whattimeisit"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/health"
              port = 8001
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }
      }
    }
  }
}