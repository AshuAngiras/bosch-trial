# What Time is it?

Is a simple web based api, that will tell you time when you want to know ;p.
it can tell you time in following formats: 
ANSIC, UnixDate, RubyDate, RFC822, RFC822Z, RFC850, RFC1123, RFC1123Z, RFC3339, RFC3339Nano, Kitchen, Stamp, StampMilli, StampMicro, StampNano
# Prerequisites
 - linuxOS (all scripts are in bash)
 - Golang 1.16
 - python3
 - Docker
 - Terraform1.13
 - kind
 - kubectl
 - curl or Postman


# How to get it working 

the service can run by itself or can be deployed on a local kubernetes cluster setup using <https://kind.sigs.k8s.io/>. Application has a very simple interface. Just send HTTP GET request and enjoy the time. 

## Running Locally

to run the application locally. simply run 

```$ make app```

this will automatically start the server at <http://localhost:8001/now>

## Running on Kubernetes
to run the application on local kubernetes, simply run 

```$ make setup```

**NOTE :  this might take a few minutes so you can go get a coffee or something.** 

This command results in creation on following resources in given order: 

 - Kind Kubernetes cluster with 1 control plane and 2 Worker nodes
 - A local docker registry service to keep track of images 
 - Nginx ingress controller within kubernetes to manage the exposed ports (this part usually takes a few minutes)
 - it will run the tests to make sure application is working accordingly
 - it will build a docker image containg the binary and will push to the local registry created in earlier steps
 - Finally it will run a terraform script that will create 
	 - Namespace : main
	 - Deployment : whattimeisit
	 - Service: whattimeisit
	 - Ingress: ingress
 - And now the application should be accesible at <http://localhost:80/now>


# External Tests

there is a python based script available that will allow you to run external test to the deployed service. Simple run 

```$ python runTest.py```

there are two tests available in this script:

 - Test1 : simple health check to see the liveness of the application
 - Test2 : test to check time and compare it with local time to see to
   the delta between the results. if the delta is more than 5secs the
   test will fail.



# Usage

send a GET request at following endpoints to get the relevant format of time 

|URL                      |      Response                      |
|-------------------------|------------------------------------|
|`/health`                |'Service is Healthy'                |
|`/now`                   |"ANSIC time formatted string"       |
|`/now?format=ansic`      |"ANSIC time formatted string"       |
|`/now?format=unixdate`    |"UnixDate time formatted string"    |
|`/now?format=rubydate`    |"RubyDate time formatted string"    |
|`/now?format=rfc822`      |"RFC822 time formatted string"      |
|`/now?format=rfc822z`     |"RFC822Z time formatted string"     |
|`/now?format=rfc850`      |"RFC850 time formatted string"      |
|`/now?format=rfc1123`     |"RFC1123 time formatted string"     |
|`/now?format=rfc1123z`    |"RFC1123Z time formatted string"    |
|`/now?format=rfc3339`     |"RFC3339 time formatted string"     |
|`/now?format=rfc3339nano` |"RFC3339Nano time formatted string" |
|`/now?format=kitchen`     |"Kitchen time formatted string"     |
|`/now?format=stamp`       |"Stamp time formatted string"       |
|`/now?format=stampmilli`  |"StampMilli time formatted string"  |
|`/now?format=stampmicro`  |"StampMicro time formatted string"  |
|`/now?format=stampnano`   |"StampNano time formatted string"   |

