package service

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/AshuAngiras/bosch-trial/pkg/api"
)

func Start() {

	r := mux.NewRouter()

	// handlers
	r.HandleFunc("/now", api.TimeNow).Methods(http.MethodGet)
	r.HandleFunc("/health", api.HealthCheck).Methods(http.MethodGet)

	r.Use(mux.CORSMethodMiddleware(r))
	log.Println("Starting server at :8001")

	err := http.ListenAndServe(":8001", handlers.CombinedLoggingHandler(os.Stdout, r))
	if err != nil {
		log.Printf("Service stopped: %s\n", err)
	}
	return
}
