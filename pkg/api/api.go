package api

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/AshuAngiras/bosch-trial/internal"
)

// HealthCheck HealthCheck
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "service is Healthy :)")
}

// TimeNow handler to return the current time with requested time format
func TimeNow(w http.ResponseWriter, r *http.Request) {

	format, ok := r.URL.Query()["format"]
	var finalformat string

	if !ok || len(format[0]) < 1 {
		log.Println("requested time format is either is missing or invalid")
		finalformat = "ANSIC"
	} else {
		finalformat = format[0]
	}

	// call internal package for current time
	timeRightNow, err := internal.GetTime(finalformat)

	if err == nil {
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, timeRightNow)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "invalid time format")

	}
	return
}
