# Testing Public API for Sanity 
import requests 
import time
from datetime import datetime as dt

Test_URL = "http://localhost:80"
  

## Test 1 ###
print("\n-------------- Test 1 - Health Check --------------\n")
r = requests.get(url = Test_URL+"/health") 

if r.status_code == 200 : 
    print("Status Code recieved = ",r.status_code)
    print("Test 1 Passed")
else :
    print("Test 1 Failed")
    exit(1)


## Test 2 ###
print("\n-------------- Test 2 - Checking time --------------\n")
r = requests.get(url = Test_URL+"/now") 
if r.status_code == 200 : 
    print("Status Code recieved = ",r.status_code)
    print("Data recieved : ",r.text)
    local_time = dt.now()
    print("Local time:", local_time.ctime())	

    received = dt.strptime(r.text,'%a %b %d %H:%M:%S %Y')

    delta = (received-local_time).total_seconds()
    print("Delta between two times : ",delta,"secs")
    
    if abs(delta) > 5 :
        print("Test 2 Failed")
    else :
        print("Test 2 Pass")
else :
    print("Test 2 Failed")
    exit(3)
