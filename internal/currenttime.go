package internal

import (
	"errors"
	"strings"
	"time"
)

func GetTime(format string) (string, error) {

	currentTime := time.Now()

	var timeRightNow string

	format = strings.ToLower(format)

	switch format {
	case "ansic":
		timeRightNow = currentTime.Format(time.ANSIC)
		break
	case "unixdate":
		timeRightNow = currentTime.Format(time.UnixDate)
		break
	case "rubydate":
		timeRightNow = currentTime.Format(time.RubyDate)
		break
	case "rfc822":
		timeRightNow = currentTime.Format(time.RFC822)
		break
	case "rfc822z":
		timeRightNow = currentTime.Format(time.RFC822Z)
		break
	case "rfc850":
		timeRightNow = currentTime.Format(time.RFC850)
		break
	case "rfc1123":
		timeRightNow = currentTime.Format(time.RFC1123)
		break
	case "rfc1123z":
		timeRightNow = currentTime.Format(time.RFC1123Z)
		break
	case "rfc3339":
		timeRightNow = currentTime.Format(time.RFC3339)
		break
	case "rfc3339nano":
		timeRightNow = currentTime.Format(time.RFC3339Nano)
		break
	case "kitchen":
		timeRightNow = currentTime.Format(time.Kitchen)
		break
	case "stamp":
		timeRightNow = currentTime.Format(time.Stamp)
		break
	case "stampmilli":
		timeRightNow = currentTime.Format(time.StampMilli)
		break
	case "stampmicro":
		timeRightNow = currentTime.Format(time.StampMicro)
		break
	case "stampnano":
		timeRightNow = currentTime.Format(time.StampNano)
		break
	default:
		return "", errors.New("Invalid time format")
	}
	return timeRightNow, nil
}
